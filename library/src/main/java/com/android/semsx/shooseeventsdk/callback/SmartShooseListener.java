package com.android.semsx.shooseeventsdk.callback;

import android.support.annotation.UiThread;

import com.android.semsx.shooseeventsdk.event.Event;

/**
 * 回调
 */
public interface SmartShooseListener {

    /**
     * 接收事件
     *
     * @param event 事件
     */
    @UiThread
    void onEvent(Event event);

}
