package com.android.semsx.shooseeventsdk;

import com.android.semsx.shooseeventsdk.callback.SmartShooseListener;
import com.android.semsx.shooseeventsdk.event.Event;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * The Smart shoose magager.
 */
public enum SmartShooseMagager {

    INSTANCE;

    private final ConnectableObservable<Event> mConnectableObservable;
    private final PublishSubject<Event> mPublishSubject = PublishSubject.create();

    SmartShooseMagager() {
        mConnectableObservable = mPublishSubject
                .publish();
    }

    public void triggerEvent(Event event) {
        mPublishSubject.onNext(event);
    }

    /**
     * Subscribe.
     *
     * @param listener the listener
     */
    public Subscription subscribe(final String eventName, final SmartShooseListener listener) {
        mConnectableObservable.connect();
        return mConnectableObservable
                .filter(new Func1<Event, Boolean>() {
                    @Override
                    public Boolean call(Event event) {
                        return event.getEventName().equals(eventName);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(Event event) {
                        listener.onEvent(event);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

}
