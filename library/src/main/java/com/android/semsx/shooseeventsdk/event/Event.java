package com.android.semsx.shooseeventsdk.event;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * 事件
 */
public class Event {

    @NonNull
    private final String mEventName;
    @NonNull
    private final Map<String, Object> mEventParam;

    public Event(@NonNull String eventName, @NonNull Map<String, Object> eventParam) {
        mEventName = eventName;
        mEventParam = eventParam;
    }

    @NonNull
    public String getEventName() {
        return mEventName;
    }

    @NonNull
    public Map<String, Object> getEventParam() {
        return mEventParam;
    }
}
