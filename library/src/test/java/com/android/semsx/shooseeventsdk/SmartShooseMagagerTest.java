package com.android.semsx.shooseeventsdk;

import android.util.Log;

import com.android.semsx.shooseeventsdk.callback.SmartShooseListener;
import com.android.semsx.shooseeventsdk.event.Event;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * The type Smart shoose magager test.
 */
@RunWith(RobolectricTestRunner.class)
public class SmartShooseMagagerTest {

    @Rule
    public RxSchedulersOverrideRule mRxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Test
    public void testSubscribe() throws Exception {
        SmartShooseMagager.INSTANCE.subscribe("TEST", new SmartShooseListener() {
            @Override
            public void onEvent(Event event) {
                Map<String, Object> eventParam = event.getEventParam();
                assertEquals(eventParam.get("param"), "value");
            }
        });
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("param", "value");
        SmartShooseMagager.INSTANCE.triggerEvent(new Event("TEST", hashMap));
    }

    @Test
    public void testSubscribeError() throws Exception {
        SmartShooseMagager.INSTANCE.subscribe("TEST", new SmartShooseListener() {
            @Override
            public void onEvent(Event event) {
                throw new IllegalStateException();
            }
        });
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("param", "value");
        SmartShooseMagager.INSTANCE.triggerEvent(new Event("TEST", hashMap));
    }

}