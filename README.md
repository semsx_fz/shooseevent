# SDK使用方
## 依赖
使用方增加远端依赖
```Groovy
apply from: "https://bitbucket.org/semsx_fz/smartshoosedep/raw/master/smartshoose.dep.gradle"
```
当远端发布新版本时将自动下载新版依赖包更新
## 事件监听
### 注册
以下代码监听TestEvent事件，并且从Event参数获取结果，具体事件见实现方SDK
```Java
mSubscription = SmartShooseMagager.INSTANCE
                .subscribe("TestEvent", new SmartShooseListener() {
                    @Override
                    public void onEvent(Event event) {
                        Log.d("MainActivity", "event.getEventParam():" + event.getEventParam());
                    }
                });
```
### 反注册
注册需要搭配反注册防止内存泄漏
```Java
mSubscription.unsubscribe()
```
# SDK事件实现方
dep.gradle寄存在Bitbucket，为了让使用方及时使用到最新版本的SDK，实现方SDK如有新版本需提交最新版本到依赖Repo
- 注册Bitbucket账号
- 并提交给管理员添加邮箱到[Repo](https://bitbucket.org/semsx_fz/smartshoosedep)
- 发布新版本
- 提交新的依赖版本到上述Repo

## 发送事件
[![](https://jitpack.io/v/org.bitbucket.semsx_fz/shooseevent.svg)](https://jitpack.io/#org.bitbucket.semsx_fz/shooseevent)

### 依赖ShooseEvent
```Groovy
compile 'org.bitbucket.semsx_fz:shooseevent:0.2.0'
```
### 发送事件
```Java
Map<String, Object> map = new HashMap<>();
map.put("Speed", 1.2);
SmartShooseMagager.INSTANCE.triggerEvent(new Event("TestEvent", map));
```
